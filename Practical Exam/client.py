import socket
from threading import Thread
import json

def sendMsg(client, server):
	while True:
		msg = input("")
		if msg == "QUIT":
			server.send(msg.encode())
			break
		client.sendto(msg.encode(), ('<broadcast>', 6666))
		server.send(msg.encode())
		###
		"""server_msg, addr = server.recvfrom(2048)
		if server_msg.decode() == "QUIT":
			print("Quitting listening from server..")
			client.sendto(server_msg, ('<broadcast>', 6666))
			break
		clients = json.loads(server_msg)
		print("Message received from server:", clients)
		for c in clients:
			if 
			client.sendto(msg.encode(), (c[0], c[1]))"""

def listenMsg(client):
	while True:
		msg, addr = client.recvfrom(2048)
		if msg.decode() == "QUIT":
			break
		print("Message received from client:", msg.decode())

def listenToServer(client, server):
	clients_copy = []
	while True:
		msg, addr = server.recvfrom(2048)
		if msg.decode() == "QUIT":
			print("Quitting listening from server..")
			client.sendto(msg, ('<broadcast>', 6666))
			break
		clients = json.loads(msg)
		if clients != clients_copy:
			print("Someone left or joined")
		clients_copy = clients
		print("Message received from server:", clients)

def client():
	# server
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(("localhost", 7000))

	# client
	s2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s2.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	s2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s2.setblocking(True)
	s2.bind(("0.0.0.0", 6666))

	send = Thread(target=sendMsg, args=[s2, s])
	send.start()

	lis = Thread(target=listenMsg, args=[s2])
	lis.start()

	serv = Thread(target=listenToServer, args=[s2, s])
	serv.start()

	send.join()
	lis.join()
	serv.join()

	print("Connection closed.")
	s.close()
	s2.close()


if __name__ == '__main__':
	client()