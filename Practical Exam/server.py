import select
import socket
import sys
import queue
import json

if __name__ == '__main__':
	try:
		server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server.setblocking(0)
		server.bind(("localhost", 7000))
		server.listen(5)
	except socket.error as msg:
		print(msg)
		exit(-1)

	inputs = [server]
	outputs = []
	message_queues = {}
	clients = []

	while inputs:
	    #print("Waiting for at least one socket to be ready..")
	    readable, writable, exceptional = select.select(inputs, outputs, inputs)
	    #print("Waiting is done")
	    for s in readable:
	        if s is server:
	            # A "readable" server socket is ready to accept a connection
	            connection, client_address = s.accept()
	            clients.append(client_address)
	            print("Clients connected:", clients)
	            connection.setblocking(0)
	            inputs.append(connection)

	            # Give the connection a queue for data we want to send
	            message_queues[connection] = queue.Queue()
	        else:
	            data = s.recv(1024)
	            if data:
	                # A readable client socket has data
	                if data.decode() == "QUIT":
	                	print("Quitting connection with client..")
	                	ip, port = s.getpeername()
	                	for i in range(0, len(clients)):
	                		if clients[i][0] == ip and clients[i][1] == port:
	                			del clients[i]
	                	print("Clients remained:", clients)

	                	s.send("QUIT".encode())
	                	if s in outputs:
	                		outputs.remove(s)
	                	inputs.remove(s)
	                	s.close()
	                	del message_queues[s]
	                else:
		                print("Received from client:", data.decode())
		                message_queues[s].put(json.dumps(clients).encode())

		                # Add output channel for response
		                if s not in outputs:
		                    outputs.append(s)

	     # Handle outputs
	    for s in writable:
	        try:
	            next_msg = message_queues[s].get_nowait()
	        except queue.Empty:
	            # No messages waiting so stop checking for writability.
	            #print("Output queue is empty for", s.getpeername())
	            outputs.remove(s)
	        else:
	        	print("Sending to client:", next_msg.decode())
	        	s.send(next_msg)

	    # Handle "exceptional conditions"
	    for s in exceptional:
	        print("Handling exceptions for", s.getpeername())
	        # Stop listening for input on the connection
	        inputs.remove(s)
	        if s in outputs:
	            outputs.remove(s)
	        s.close()

	        # Remove message queue
	        del message_queues[s]