import select
import socket
import sys
import queue
import string, random
import json

if __name__ == '__main__':
	try:
		server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		server.setblocking(0)
		server.bind(("localhost", 5555))
		server.listen(5)
	except socket.error as msg:
		print(msg)
		exit(-1)

	inputs = [server]
	outputs = []
	message_queues = {}

	while inputs:
	    #print("Waiting for at least one socket to be ready..")
	    readable, writable, exceptional = select.select(inputs, outputs, inputs)
	    #print("Waiting is done")
	    for s in readable:
	        if s is server:
	            # A "readable" server socket is ready to accept a connection
	            connection, client_address = s.accept()
	            print("New connection from:", client_address)
	            connection.setblocking(0)
	            inputs.append(connection)

	            # Give the connection a queue for data we want to send
	            message_queues[connection] = queue.Queue()
	        else:
	            data = s.recv(1024)
	            if data:
	                # A readable client socket has data
	                print("Received from leader:", data.decode())
	                random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(7))
	                random_string += '.'
	                message_queues[s].put(random_string.encode())

	                random_list = []
	                random_list_size = random.randint(0, 10)
	                for i in range(0, random_list_size):
	                	random_list.append(random.randint(0, 1000))
	                message_queues[s].put(json.dumps(random_list).encode())

	                # Add output channel for response
	                if s not in outputs:
	                    outputs.append(s)
	            else:
	                # Interpret empty result as closed connection
	                print("Closing leader after reading no data", client_address)
	                # Stop listening for input on the connection
	                if s in outputs:
	                    outputs.remove(s)
	                inputs.remove(s)
	                s.close()

	                # Remove message queue
	                del message_queues[s]

	     # Handle outputs
	    for s in writable:
	        try:
	            next_msg = message_queues[s].get_nowait()
	        except queue.Empty:
	            # No messages waiting so stop checking for writability.
	            print("Output queue is empty for", s.getpeername())
	            outputs.remove(s)
	        else:
	        	print("Sending to leader:", next_msg.decode())
	        	s.send(next_msg)

	    # Handle "exceptional conditions"
	    for s in exceptional:
	        print("Handling exceptions for", s.getpeername())
	        # Stop listening for input on the connection
	        inputs.remove(s)
	        if s in outputs:
	            outputs.remove(s)
	        s.close()

	        # Remove message queue
	        del message_queues[s]