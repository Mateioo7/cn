import socket
import time
import string, random
from threading import Thread

def notifyStudents(s, group_port):
	while True:
		print("Notifying students I'm the leader..")
		s.sendto("leader".encode(), ('<broadcast>', group_port))
		time.sleep(5)

def solveQuestion(s, group_port):
	try:
		#teacher socket
		ts = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# for linux
		#ts.connect(("192.168.0.188", 5555))
		# for local
		ts.connect(("localhost", 5555))
	except socket.error as msg:
		print(msg)
		ts.close()
		exit(-1)

	while True:
		student_question, addr = s.recvfrom(1024)
		print("Leader sending student question to teacher:", student_question.decode())
		# for linux
		#ts.send(student_question)
		# for local
		ts.send(student_question)

		teacher_msg1, addr = ts.recvfrom(1024)
		teacher_msg2, addr = ts.recvfrom(1024)
		print("Leader received from teacher:", teacher_msg1.decode(), teacher_msg2.decode())
		print("Sending teacher response to students..")
		s.sendto(teacher_msg1, ('<broadcast>', group_port))
		s.sendto(teacher_msg2, ('<broadcast>', group_port))


def leader(group_port):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.setblocking(True)
		s.bind(("0.0.0.0", 6666))
	except socket.error as msg:
		print(msg)
		s.close()
		exit(-1)

	notify = Thread(target=notifyStudents, args=(s, group_port))
	notify.start()

	solve = Thread(target=solveQuestion, args=(s, group_port))
	solve.start()

def listenLeader(s):
	while True:
		leader_msg, addr = s.recvfrom(1024)
		if (leader_msg.decode() == "leader"):
			print("Leader group sent:", leader_msg.decode())
		else:
			print("Leader respone from teacher:", leader_msg.decode())

def askQuestion(s, leader_addr):
	while True:
		value = random.uniform(0, 1)
		print("Computed random value:", str(value))
		if value > 0.5:
			random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(7))
			random_string += '?'
			print("Sending random question:", random_string)
			s.sendto(random_string.encode(), leader_addr)
		time.sleep(3)

def student(group_port):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.bind(("0.0.0.0", group_port))
	except socket.error as msg:
		print(msg)
		s.close()
		exit(-1)

	print("Waiting for the leader address..")
	data, leader_addr = s.recvfrom(1024)

	listen = Thread(target=listenLeader, args=[s])
	listen.start()

	ask = Thread(target=askQuestion, args=[s, leader_addr])
	ask.start()


if __name__ == '__main__':
	group_port = int(input("Give group port: "))
	group_port += 5000
	isLeader = int(input("Are you a leader? 1-yes, 0-no: "))

	if isLeader == 1:
		leader(group_port)
	elif isLeader == 0:
		student(group_port)