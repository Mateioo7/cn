import socket, select


if __name__ == '__main__':
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect(("192.168.0.188", 5555))
	except socket.error as msg:
		print(msg)
		exit(-1)

	s.send("hello select server".encode())

	msg = s.recv(4096)
	print("server respone:", msg.decode())

	s.close()