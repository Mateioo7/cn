import socket
import random

ip = "192.168.0.188"
port = 5555
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

s.sendto(str(random.randint(0, 9)).encode(), (ip, port))

b_msg, si_addr = s.recvfrom(32)
print("server: " + str(b_msg.decode()))

s.close()