import socket
import time
from multiprocessing import Process, Value
import random

def f(s, val, si_addr, i, guessed):
    try:
        print("client " + str(i) + ": " + str(val))

        if val == rInt:
            guessed.value = 1
            s.sendto(("You found it client " + str(i) + "!").encode(), si_addr)
        else:
            s.sendto(("Try again client " + str(i)).encode(), si_addr)
    except socket.error as msg:
        print(msg)
        exit(-1)

def main():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("0.0.0.0", 5555))
    except socket.error as msg:
        print(msg)
        exit(-1)

    i = 0
    guessed = Value('i', 0)
    while guessed.value == 0:
        b_msg, si_addr = s.recvfrom(5)
        val = int(b_msg.decode())
        i += 1

        p = Process(target=f, args=(s, val, si_addr, i, guessed))
        p.start()
        p.join()


    print("End of the game, client " + str(i) + " found the lucky" +
        " number: " + str(rInt))
    s.close()

if __name__ == '__main__':
    rInt = random.randint(0, 9)
    print("Chosen digit: " + str(rInt))
    main()
