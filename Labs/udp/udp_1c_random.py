import socket
import random
import secrets

random.seed()
start = 0
end = 2**12

if __name__ == '__main__':
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	except socket.error as msg:
		print(msg)
		s.close()
		exit(-1)

	steps = 0
	guessed = False
	while not guessed:
		try:
			#val = secrets.randbelow(end)
			val = random.randint(start, end)
			s.sendto(str(val).encode(), ("192.168.0.188", 5555))
			b_msg, si_addr = s.recvfrom(32)
		except socket.error as msg:
			print(msg)
			s.close()
			exit(-1)

		steps += 1
		msg = str(b_msg.decode())
		if msg == 'G':
			guessed = True
		elif msg == 'H':
			start = val
		elif msg == 'S':
			end = val
		print("Step", str(steps), "client sent", str(val), "received", str(msg))

	s.close()

	
	print("I guessed", str(val), "in", str(steps), "steps.")