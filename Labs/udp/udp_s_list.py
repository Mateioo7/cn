import socket
import json

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("0.0.0.0", 5555))

b_msg, si_add = s.recvfrom(30)
msg = b_msg.decode()
print("windows add: " + si_add[0] + " " + str(si_add[1]))
print("windows msg: " + msg)

l = [1,2,7,8]
s.sendto(json.dumps(l).encode(), si_add)
