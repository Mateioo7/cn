import socket
import json

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# this bind is optional
s.bind(("0.0.0.0", 5556))
# 192.168.0.188 in ubuntu
s.sendto("hey".encode(), ("192.168.0.199", 5555))

b_msg, si_add = s.recvfrom(30)
msg = b_msg.decode()
print("ubuntu add: " + si_add[0] + " " + str(si_add[1])) 
print("received from ubuntu: " + msg)

list = json.loads(msg)
s = 0
for e in list:
	s += e
print("sum: " + str(s))