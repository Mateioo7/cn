import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# line below required so we can reuse the same address instantly
# otherwise, I'd have to wait 90 secs before starting the server
# with the same address (which is what I do)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(("0.0.0.0", 5555))
s.listen(1)

io, si_add = s.accept()
print("client address: " + si_add[0] + " " + str(si_add[1]))

b_msg = io.recv(20)
print("msg from client: " + b_msg.decode())
io.send("hello back".encode())
io.close()
s.close()
