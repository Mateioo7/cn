import socket

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.0.188", 5555))

s.send("hello".encode())

msg = s.recv(20)
print("msg from server: " + msg.decode())

s.close()