import socket
import random

try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(("192.168.0.188", 5555))
except socket.error as msg:
	print(msg)
	exit(-1)

rInt = random.randint(0, 9)
s.send(str(rInt).encode())

b_msg = s.recv(32)
s_msg = b_msg.decode()
print("server: " + s_msg)

s.close()