import socket
import time
from threading import Thread, Lock
import random
import sys

guessed = False
lock = Lock()
rInt = random.randint(0,9)

def f(io, i):
    global guessed

    try:
        b_msg = io.recv(5)
        msg = b_msg.decode()
        print("client " + str(i) + ": " + msg)

        #time.sleep(5)

        if int(msg) == rInt:
            lock.acquire()
            guessed = True
            lock.release()
            io.send(("You found it client " + str(i) + "!").encode())
        else:
            io.send(("Try again client " + str(i)).encode())
    except socket.error as msg:
        print(msg.strerror)

    io.close()
    sys.exit()

def main():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("0.0.0.0", 5555))
        s.listen(5)
    except socket.error as msg:
        print(msg.strerror)
        exit(-1)

    i = 0

    while not guessed:
        io, si_add = s.accept()
        i += 1

        t = Thread(target=f, args=(io,i,))
        t.start()
        t.join()

    print("End of the game, client " + str(i) + " found the lucky" +
        " number: " + str(rInt))
    s.close()

if __name__ == '__main__':
    main()
