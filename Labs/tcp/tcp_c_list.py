import socket
import json

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# in ubuntu I had 192.168.0.188
s.connect(("192.168.0.199", 5555))
s.send("hello".encode())

b_msg = s.recv(20)
s_l = b_msg.decode()
print("msg from server: " + s_l)

l = json.loads(s_l)
sum = 0
for e in l:
	sum += e
print("sum from server list: " + str(sum))
s.close()